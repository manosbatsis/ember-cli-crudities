import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('record-line', 'Integration | Component | record line', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const deleteRecord = function() {};
  this.set('deleteRecord', deleteRecord);
  this.render(hbs`{{record-line deleteRecord=deleteRecord}}`);

  assert.equal(this.$().text().trim(), '');
});
