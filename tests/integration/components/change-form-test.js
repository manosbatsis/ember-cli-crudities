import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('change-form', 'Integration | Component | change form', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const model = new Ember.Object();
  this.set('model', model);
  this.render(hbs`{{change-form model=model}}`);

  const result = this.$().text().trim().replace(/ *\n*/g, '')
  assert.ok(result.match(/Save/));
  assert.ok(result.match(/Saveandcontinueediting/));
  assert.ok(result.match(/Delete/));
  assert.ok(result.match(/Cancel/));
});
