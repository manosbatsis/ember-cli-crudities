import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('form-select', 'Integration | Component | form select', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const extra = new Ember.Object({
    choices: new Ember.A()
  });
  this.set('extra', extra);
  this.render(hbs`{{form-select extra=extra}}`);

  assert.equal(this.$().text().trim(), '---');
});
