import { queryString } from 'dummy/helpers/query-string';
import { module, test } from 'qunit';
import Ember from 'ember';

module('Unit | Helper | query string');

// Replace this with your real tests.
test('it works', function(assert) {
  let data = [
    new Ember.Object({id: 1}),
    new Ember.Object({id: 2}),
    new Ember.Object({id: 3})
  ];
  let result = queryString([data]);
  assert.equal(result, 'ids[]=1&ids[]=2&ids[]=3');
});
