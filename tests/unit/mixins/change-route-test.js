import Ember from 'ember';
import ChangeRouteMixin from 'ember-cli-crudities/mixins/change-route';
import { module, test } from 'qunit';

module('Unit | Mixin | change route');

// Replace this with your real tests.
test('it works', function(assert) {
  let ChangeRouteObject = Ember.Object.extend(ChangeRouteMixin);
  let subject = ChangeRouteObject.create();
  assert.ok(subject);
});
