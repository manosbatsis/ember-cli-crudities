import Ember from 'ember';
import WithCustomActionsMixin from 'ember-cli-crudities/mixins/with-custom-actions';
import { module, test } from 'qunit';

module('Unit | Mixin | with custom actions');

// Replace this with your real tests.
test('it works', function(assert) {
  let WithCustomActionsObject = Ember.Object.extend(WithCustomActionsMixin);
  let subject = WithCustomActionsObject.create();
  assert.ok(subject);
});
