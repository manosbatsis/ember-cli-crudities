import Ember from 'ember';
import BoundvalueMixin from 'ember-cli-crudities/mixins/boundvalue';
import { module, test } from 'qunit';

module('Unit | Mixin | boundvalue');

// Replace this with your real tests.
test('it works', function(assert) {
  let BoundvalueObject = Ember.Object.extend(BoundvalueMixin);
  let subject = BoundvalueObject.create();
  assert.ok(subject);
});
