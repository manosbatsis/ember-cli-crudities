import Ember from 'ember';
import ToManyStack from './form-tomany-stack';
import layout from '../templates/components/form-tomany-table';

export default ToManyStack.extend({
  layout,

  column_count: Ember.computed('field.fields.length', 'actionColumns', function() {
    const readonly = this.get('readonly');
    return parseInt(this.get('field.fields.length')) + 
      parseInt(readonly !== true ? 1 : 0) +
      parseInt(this.get('field.extra.sortableBy') ? 1 : 0) +
      this.get('actionColumns');
  }),
});
