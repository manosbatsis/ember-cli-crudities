import Ember from 'ember';
import layout from '../templates/components/record-line';
import SortableItem from 'ember-sortable/mixins/sortable-item';
import withCustomActionsMixin from '../mixins/with-custom-actions';

export default Ember.Component.extend(SortableItem, withCustomActionsMixin, {
  layout,
  tagName: 'tr',
  classNameBindings: ['state'],
  timer: undefined,

  state: '',

  actions: {
    addNew() {

    },
    save(/* model, field */) {
      Ember.run.debounce(this, () => {
        this.get('model').save().then(() => {
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'success');
          this.timer = Ember.run.later(this, () => {
            this.set('state', '');
          }, 750);
        }).catch((err) => {
          /* eslint-disable no-console */
          console.error(err);
          /* eslint-enable no-console */
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'danger');
          this.timer = Ember.run.later(this, () => {
            this.set('state', '');
          }, 750);
        });
      }, 250);
    },
  }
});
