import Ember from 'ember';
import layout from '../templates/components/conditional-display';
import ConditionEvaluatorMixin from '../mixins/condition-evaluator';

export default Ember.Component.extend(ConditionEvaluatorMixin, {
  layout,

  display: Ember.computed('condition', 'value', function() {
    const condition = this.get('condition');
    if (condition === undefined) {
      return true;
    }

    const value = this.get('value');
    return this.evaluate(condition, value);
  }),
});
