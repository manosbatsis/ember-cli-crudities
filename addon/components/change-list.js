import Ember from 'ember';
import { task } from 'ember-concurrency';
import layout from '../templates/components/change-list';
import withCustomActionsMixin from '../mixins/with-custom-actions';
import ConditionEvaluatorMixin from '../mixins/condition-evaluator';

export default Ember.Component.extend(withCustomActionsMixin, ConditionEvaluatorMixin, {
  layout,
  classNames: ['row'],
  store: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  ordering_fields: [],
  filter_fields: [],
  custom_actions: [],
  bulk_actions: [],
  search_enabled: false,
  _search_value: null,
  filterValues: new Ember.Object(),
  _filter_values: {},
  filterValueChanged: 'filterValueChanged',
  sortBy: ['id'],
  sorted: Ember.computed.sort('model', 'sortBy'),
  
  modalOpen: false,
  modalMessage: undefined,

  orderable: Ember.computed('sortableBy', 'filterValues.ordering', function() {
    const sortableBy = this.get('sortableBy');
    if (!sortableBy) {
      return false;
    }
    const ordering = this.get('filterValues.ordering');
    if (Ember.isEmpty(ordering)) {
      return true;
    }
    return sortableBy === ordering;
  }),

  searchValue: Ember.computed('_search_value', {
    get() {
      return this.get('_search_value');
    },
    set(key, value) {
      this.set('_search_value', value);
      this.filterModel(value, this.get('_filter_values'), 1);
      return value;
    }
  }),

  filterFields: Ember.computed('filter_fields', 'filter_fields[]', function() {
    const fields = this.get('fields');
    const filter_fields = this.get('filter_fields');
    const rv = Ember.A();

    filter_fields.forEach((field_name) => {
      field_name = field_name.replace(/_id$/, '');
      const filtered = fields.filterBy('name', field_name);
      if (filtered.length > 0) {
        const field = Ember.copy(filtered[0]);
        Ember.set(field, 'readonly', false);
        Ember.set(field, 'required', false);
        Ember.set(field, 'horiClass', 'col-xs-12');
        Ember.set(field, 'inputClass', 'col-xs-12');
        if (field.extra === undefined) {
          Ember.set(field, 'extra', {});
        }
        Ember.set(field, 'extra.clearable', true);
        rv.push(field);
      }
    });
    return rv;
  }),

  showFilters: Ember.computed('search_enabled', 'filter_fields', 'filter_fields[]', function() {
    return this.attrs.model === undefined && (this.get('search_enabled') || this.get('filter_fields.length') > 0);
  }),

  listClassName: Ember.computed('showFilters', function() {
    if (this.get('showFilters') === true) {
      return 'col-sm-12 col-md-8 col-lg-9 col-md-pull-4 col-lg-pull-3';
    }
    return 'col-xs-12';
  }),

  fields: [
    {
      'name': '__str__',
      'widget': 'text',
      'read_only': true,
      'label': 'Object'
    }
  ],
  list_display: [
    '__str__'
  ],
  list_editable: [],

  display_fields: Ember.computed('list_display[]', 'fields[]', 'list_display', 'fields', 'list_editable', 'list_editable[]', function() {
    const fields = this.get('fields');
    const list_display = this.get('list_display');
    const list_editable = this.get('list_editable');
    const rv = Ember.A();

    if (list_display) {
      list_display.forEach((field_name) => {
        const filtered = fields.filterBy('name', field_name);
        if (filtered.length > 0) {
          const field = Ember.copy(filtered[0]);
          if (list_editable.indexOf(field_name) === -1) {
            Ember.set(field, 'readonly', true);
          }
          rv.push(field);
        }
      });
    }

    return rv;
  }),

  column_count: Ember.computed('list_display', 'list_display.length', 'custom_actions.length', 'changeFormRoute', 'app_name', 'label', 'orderable', function() {
    const rv = parseInt(this.get('list_display.length')) +
           parseInt(this.get('custom_actions.length')) +
           2 +
           parseInt(this.get('changeFormRoute') && this.get('app_name') && this.get('label') ? 1 : 0) +
           parseInt(this.get('orderable') ? 1 : 0)
    ;
    return rv;
  }),

  title: Ember.computed('label', function() {
    const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);
    const label = this.get('label');
    return inflector.pluralize(label);
  }),

  filterModel(search, filters, pageNumber) {
    this.set('currentPage', pageNumber);
    let rv;
    if (Ember.isEmpty(search) && filters === {} && pageNumber === 1) {
      rv = this.get('store').findAll(this.get('model_name'));
    } else {
      rv = this.get('store').query(this.get('model_name'), {search, ...filters, page: pageNumber});
    }
    rv.then((result) => {
      this.set('meta', result.get('meta'))
    });
    this.set('model', rv);
  },

  allChecked: Ember.computed('model', 'model.@each._selected', {
    get() {
      const model = this.get('model');
      return model.every((record) => {
        return record.get('_selected');
      });
    },
    set(key, value) {
      const model = this.get('model');
      model.map((record) => {
        record.set('_selected', value);
      });
      return value;
    }
  }),

  noneChecked: Ember.computed('model', 'model.@each._selected', function() {
    const model = this.get('model');
    return !model.any((record) => {
      return record.get('_selected');
    });
  }),

  selectedRecords: Ember.computed('sorted', 'sorted.length', 'sorted.@each._selected', function () {
    const records = this.get('sorted');
    return records.filter((record) => {
      return record.get('_selected');
    });
  }),

  selectedRecordsForAction: Ember.computed('selectedRecords', 'selectedBulkAction', function() {
    const condition = this.get('selectedBulkAction.display_condition');
    const selectedRecords = this.get('selectedRecords');
    if (!condition) {
      return selectedRecords;
    }

    const propertyPath = condition.property_path;
    return selectedRecords.filter((record) => {
      const value = record.get(propertyPath);
      return this.evaluate(condition, value);
    });
  }),

  ignoredRecordsForAction: Ember.computed('selectedRecordsForAction', 'selectedBulkAction', function() {
    const condition = this.get('selectedBulkAction.display_condition');
    if (!condition) {
      return null;
    }

    const selectedRecords = this.get('selectedRecords');
    const selectedRecordsForAction = this.get('selectedRecordsForAction');
    const selectIds = selectedRecordsForAction.map((record) => { return record.get('id') });
    const rv = selectedRecords.filter((record) => {
      return selectIds.indexOf(record.get('id')) === -1;
    });

    if (rv.length === 0) {
      return null;
    }

    return rv;
  }),

  bulkActions: Ember.computed('custom_actions', 'custom_actions.length', 'custom_actions.@each.allowBulk', 'bulk_actions', 'bulk_actions.length', function() {
    const rv = new Ember.A();
    rv.pushObject({
      text: 'Delete',
      type: 'closureMethod',
      method: '_do_delete_record',
      btn_class: 'btn btn-danger',
      icon_class: 'fa fa-trash',
    });

    const custom_actions = this.get('custom_actions');
    if (custom_actions) {
      custom_actions.map((action) => {
        if (action.allowBulk && action.type !== 'download') {
          rv.pushObject(action);
        }
      });
    }


    const bulk_actions = this.get('bulk_actions');
    if (bulk_actions) {
      bulk_actions.map((action) => {
        rv.pushObject(action);
      });
    }

    return rv;
  }),

  selectedBulkAction: null,

  didReceiveAttrs() {
    if (this.attrs.model !== undefined) {
      return;
    }
    this.filterModel(this.get('_search_value'), this.get('_filter_values'), 1);
    if (this.get('sortableBy')) {
      this.set('sortBy', [this.get('sortableBy')]);
    }
  },

  resetFilters: Ember.observer('model_name', function() {
    this.set('_search_value', '');
    this.set('_filter_values', {});
  }),

  _do_delete_record(record) {
    const recordName = record.get('__str__');
    return record.destroyRecord().then(() => {
      this.get('flashMessages').success(`${recordName} was sucessfully deleted`);
    }).catch((err) => {
      /* eslint-disable no-console */
      console.error(err);
      /* eslint-enable no-console */
      this.get('flashMessages').danger(`${recordName} could not be deleted`);
    });
  },

  confirm_delete: task(function * () {
    const record = this.get('modalRecord');
    yield this._do_delete_record(record).finally(() => {
      this.set('modalOpen', false);
      this.set('confirmType', undefined);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
    });
  }),

  confirm_bulk: task(function * () {
    const act = this.get('modalRecord')
    const promises = new Ember.A();
    if (act.type === 'request') {
      const { url } = act;
      this.get('selectedRecordsForAction').forEach((record) => {
        promises.pushObject(this._do_rqst(url.replace(':id', record.get('id')), act));
      });
    } else {
      this.get('selectedRecordsForAction').forEach((record) => {
        promises.pushObject(this._do_method(act, this, record));
      });
    }
    yield Ember.RSVP.Promise.all(promises).finally(() => {
      this.set('modalOpen', false);
      this.set('allChecked', false);
      this.set('confirmType', undefined);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
      this.set('selectedBulkAction', undefined);
      if (act.type === 'closureMethod') {
        this.filterModel(this.get('_search_value'), this.get('_filter_values'), this.get('currentPage'));
      }
    });
  }),

  modalButtonsDisabled: Ember.computed.alias('confirm_delete.isRunning'),

  actions: {
    filterValueChanged(property, value) {
      const filterValues = this.get('filterValues');
      const rv = {};
      const fields = this.get('filter_fields');

      fields.forEach((filter) => {
        const prop = filter.replace(/_id$/, '');
        const propVal = filterValues.get(prop);
        if (propVal !== false && !Ember.isEmpty(propVal)) {
          if (prop !== filter) {
            rv[filter] = propVal.get('id');
          } else {
            rv[prop] = propVal;
          }
        }
      });

      if (value !== false) {
        if (fields.indexOf(property) !== -1) {
          rv[property] = value;
        } else {
          if (Ember.isEmpty(value)) {
            rv[`${property}_id`] = undefined;
          } else {
            rv[`${property}_id`] = value.get('id');
          }
        }
      } else {
        rv[property] = undefined;
      }
      this.set('_filter_values', rv);

      Ember.run.debounce(this, () => {
        this.filterModel(this.get('_search_value'), rv, 1);
      }, 250);
    },

    toggleOrdering(order) {
      const currentOrdering = this.get('_filter_values.ordering');
      if (currentOrdering === order) {
        order = `-${order}`;
      }
      this.set('filterValues.ordering', order);
      this.send('filterValueChanged', 'ordering', order);
    },

    reorderItems(items) {
      items.forEach((item, index) => {
        item.set(this.get('sortableBy'), index+1);
        item.save();
      });
    },

    deleteRecord(record) {
      const model_name = this.get('model_name');
      let parts = model_name.split('/');
      this.set('confirmType', 'delete');
      this.set('modalTitle', `Confirm delete ${parts[1]} ${record.get('__str__')}`);
      this.set('modalMessage', `Please confirm you want to delete ${record.get('__str__')}`);
      this.set('modalRecord', record);
      this.set('modalOpen', true);
    },

    cancel_confirm() {
      this.set('modalOpen', false);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
    },

    bulkAction(act) {
      const model_name = this.get('model_name');
      const parts = model_name.split('/');
      let modelName = parts[1]
      let count = this.get('selectedRecordsForAction.length');
      if (count > 1) {
        const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);
        modelName = inflector.pluralize(parts[1]);
      }
      this.set('confirmType', 'bulk');
      this.set('modalTitle', `Confirm "${act.text}" ${count} ${modelName}`);
      this.set('modalMessage', `Please confirm you want to "${act.text}" ${count} ${modelName}`);
      if (this.get('ignoredRecordsForAction')) {
        count = this.get('ignoredRecordsForAction.length');
        let have, they;
        if (count > 1) {
          const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);
          modelName = inflector.pluralize(parts[1]);
          have = 'have';
          they = "they don't";
        } else {
          modelName = parts[1];
          have = 'has';
          they = "it doesn't";
        }
        this.set('ignoreMessage', `${count} ${modelName} ${have} been ignored as ${they} meet "${act.text}"'s conditions`);
      }
      this.set('modalRecord', act);
      this.set('modalOpen', true);
    },

    previousPage() {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), parseInt(this.get('currentPage')) - 1);
    },

    nextPage() {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), parseInt(this.get('currentPage')) + 1);
    },

    goToPage(page) {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), page);
    }
  },
});
