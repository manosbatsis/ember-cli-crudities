import Ember from 'ember';
import ToManyBase from './tomany-base';
import layout from '../templates/components/form-tomany-stack';

export default ToManyBase.extend({
  layout,
  sortBy: ['id'],

  actionColumns: Ember.computed('field.extra.customActions', 'field.extra.customActions.length', 'field.extra.allowOpen', function() {
    return parseInt(this.get('field.extra.customActions.length') ? this.get('field.extra.customActions.length') : 0) +
      parseInt(this.get('field.extra.allowOpen') ? 1 : 0);
  }),

  column_count: Ember.computed('field.fields.length', 'actionColumns', function() {
    const readonly = this.get('readonly');
    return parseInt(readonly !== true ? 2 : 1) +
      parseInt(this.get('field.extra.sortableBy') ? 1 : 0) +
      this.get('actionColumns');
  }),

  padderColspan: Ember.computed('field.fields.length', 'column_count', 'actionColumns', function() {
    return this.get('column_count') - parseInt(this.get('field.fields.length'))
      - this.get('actionColumns');
  }),

  sorted: Ember.computed.sort('related', 'sortBy'),

  didReceiveAttrs() {
    this._super();
    this.set('sortBy', [this.get('field.extra.sortableBy')]);
  },

  actions: {
    reorderItems(items) {
      items.forEach((item, index) => {
        item.set(this.get('field.extra.sortableBy'), index+1);
      });
    }
  },
});
