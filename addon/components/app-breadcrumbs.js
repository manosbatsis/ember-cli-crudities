import Ember from 'ember';
import layout from '../templates/components/app-breadcrumbs';

export default Ember.Component.extend({
  layout,
  tagName: 'ol',
  classNames: 'breadcrumb',
  routing: Ember.inject.service('-routing'),
  mainRoute: 'index',
  mainLabel: 'Dashboard',

  currentRoute: Ember.computed('routing.currentRouteName', function() {
    const route = this.get('routing.currentRouteName');
    let rv = this.get('mainRoute') || 'index';
    if (route === this.get('appRoute') || route === this.get('changeListRoute') || route === this.get('changeFormRoute')) {
      rv = route;
    }
    console.log('routeParams', this.get('routeParams'));
    return rv;
  }),

  routeParams: new Ember.Object({app_name: null}),
  
  routeChange: Ember.observer('routing.currentState', function() {
    console.log('routeChange', this.get('routing.currentState.emberRouter.router'));
    try {
      this.set('routeParams', this.get('routing.currentState.emberRouter.router.currentHandlerInfos')[2].params);
    } catch(e) {
      this.set('routeParams', new Ember.Object());
    }
  }),

  init() {
    this._super();
    this.routeChange();
  },
});
