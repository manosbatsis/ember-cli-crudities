import Ember from 'ember';
import layout from '../templates/components/form-file';
import BoundValueMixin from'../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,
  type: 'file',

  actions: {
    upload(ev) {
      const value = ev.target.files;
      if (value && value[0]) {
        const reader = new FileReader();
        reader.onload = (e) => {
          const data = e.target.result;
          this.set('value', data);
        };
        reader.readAsDataURL(value[0]);
      }
    },
    clear() {
      this.set('value', null);
    },
  }
});
