import Ember from 'ember';
import layout from '../templates/components/list-pagination';

export default Ember.Component.extend({
  layout,
  classNames: ['pagination', 'pull-right'],
  hasPages: Ember.computed('meta', function() {
    const meta = this.get('meta');
    if (!meta) {
      return false;
    }
    const currentPage = this.get('currentPage');
    if (meta.next && meta.next > currentPage) {
      return true;
    }
    return meta.previous;
  }),
  pageCount: Ember.computed('meta', function() {
    const next = this.get('meta.next');
    const currentPage = this.get('currentPage');
    if (!next || next < currentPage) {
      const previous = this.get('meta.previous');
      return parseInt(previous) + 1;
    }
    const records = parseInt(this.get('model.length'));
    const count = parseInt(this.get('meta.count'));
    return Math.ceil(count / records);
  }),
});
