import ForeignKeyBase from './foreignkey-base';
import layout from '../templates/components/form-manytomany-select';

export default ForeignKeyBase.extend({
  layout,
  type: 'manytomany',
});
