import Ember from 'ember';

export default Ember.Component.extend({
  modelLoader: Ember.inject.service(),

  fetch_meta: true,
  is_ready: false,
  
  __horiClass: 'col-sm-3',
  _horiClass: Ember.computed.or('horiClass', '__horiClass'),
  __inputClass: 'col-sm-9',
  _inputClass: Ember.computed.or('inputClass', '__inputClass'),

  fieldsChanged: function() {
    const rv = Ember.A();
    const innerHoriClass = this.get('_horiClass');
    const innerInputClass = this.get('_inputClass');
    const fields = this.get('field.fields');
    if (!Ember.isEmpty(fields)) {
      fields.forEach((field) => {
        if (Ember.isEmpty(field.horiClass)) {
          field.horiClass = innerHoriClass;
        }
        if (Ember.isEmpty(field.inputClass)) {
          field.inputClass = innerInputClass;
        }
        if (!Ember.isEmpty(field.target_model) && field.targetModel === undefined) {
          const target_model = this.get('store').findAll(field.model);
          field.targetModel = target_model;
        }
        rv.pushObject(new Ember.Object(field));
      });
    }
    this.set('fields', rv);
    this.get('model').addObserver('content.isLoaded', this._update_ready.bind(this));
    this._update_ready();
  },

  _update_ready() {
    const loaded = this.get('model.content.isLoaded');
    if (loaded && !this.get('isDestroyed')) {
      this.set('is_ready', true);
      this.get('model').removeObserver('content.isLoaded', this._update_ready.bind(this));
    }
  },

  willDestroyElement() {
    this.get('model').removeObserver('content.isLoaded', this._update_ready.bind(this));
  },

  save() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.save();
      });
    }
  },

  delete() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.destroyRecord();
      });
    }
  },

  _fetchMeta(model) {
    if (this.get('fetch_meta') === true) {
      try {
        let fieldset = this.get('field');
        const modelLoader = this.get('modelLoader');
        const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);

        let model_name = model.get('content._internalModel.type.modelName');
        if (model_name === undefined) {
          model_name = model.get('content.type.modelName');
        }
        if (model_name === undefined) {
          model_name = model.get('_internalModel.type.modelName');
        }
        if (model_name === undefined) {
          model_name = model.get('type.modelName');
        }
        if (model_name === undefined) {
          throw "Undefined model_name";
        }
        let plural_name = model_name.split('/');
        plural_name[1] = inflector.pluralize(plural_name[1]);

        modelLoader.get_model_meta(plural_name[0], plural_name[1]).then((meta) => {
          const merged = modelLoader.merge_fields_fieldset(meta.fields, fieldset);
          if (!(this.get('isDestroyed')) && !Ember.isEmpty(fieldset.fields)) {
            Ember.set(fieldset, 'fields', merged.fields);
            const def = {};
            meta.fields.forEach((field) => {
              const field_default = Ember.get(field, 'extra.default');
              if (field_default) {
                def[Ember.get(field, 'name')] = field_default;
              }
            });
            this.set('_defaults', def);
            this.set('field', fieldset);
            this.set('fetch_meta', false);
            this.fieldsChanged();
          } else if (Ember.isEmpty(fieldset.fields)) {
            console.error('Empty fieldset for', plural_name);
          }
        }, (err) => {
          console.error('while loading meta', err);
        });
      } catch (e) {
        /* eslint-disable no-console */
        console.error('undefined model_name');
        /* eslint-enable no-console */
      }
    }  else {
      this.fieldsChanged();
    }
  },

  didReceiveAttrs() {
    const model = this.get('model');
    if (model !== undefined && model.hasOwnProperty('isFulfilled')) {
      this.get('model').then((model) => {
        this._fetchMeta(model);
      });
    } else if (model !== undefined) {
      this._fetchMeta(model);
    } else {
      console.error(model, 'is undefined', this.get('field.name'));
    }
  },
});
