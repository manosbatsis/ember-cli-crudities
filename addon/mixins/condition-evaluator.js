import Ember from 'ember';

export default Ember.Mixin.create({
  evaluate(condition, value) {
    let rv = false;
    switch(condition.operator) {
      case 'eq':
        rv = value === condition.value;
        break;
      case 'not-eq': 
        rv = value !== condition.value;
        break;
      case 'not':
        rv = !value
        break;
      case 'and':
        rv = value && condition.value;
        break;
      case 'or':
        rv = value || condition.value;
        break;
      case 'xor':
        rv = value && !condition.value || !value && condition.value;
        break;
      case 'gt':
        rv = value > condition.value;
        break;
      case 'gte':
        rv = value >= condition.value;
        break;
      case 'lt':
        rv = value < condition.value;
        break;
      case 'lte':
        rv = value <= condition.value;
        break;
      case 'is-array':
        rv = Ember.isArray(value);
        break;
      case 'is-equal':
        rv = Ember.isEqual(value, condition.value);
        break;
      case 'in':
        rv = condition.value.indexOf(value) > -1;
        break;
      case 'not-in':
        rv = condition.value.indexOf(value) === -1;
        break;
    }

    if (condition.type === 'unless') {
      rv = !rv;
    }

    return rv;
  }
});
