import Ember from 'ember';

export default Ember.Mixin.create({

  original: undefined,

  // init() {
  //   this._super();
  //   Ember.Binding.from('model.' + this.get('property')).to('value').connect(this);
  // },

  didReceiveAttrs() {
    if (this.get('model.hasDirtyAttributes') && !this.get('model.isNew')) {
      // this.get('model').rollbackAttributes();
      console.error('the model was updated somewhere', this.get('model'));
    }
    this.set('original', this.get(`model.${this.get('property')}`));
  },

  onValueChange: Ember.observer('value', function() {
    if (this.attrs.onChange !== undefined) {
      let original = this.get('original');
      try {
        original = original.get('id');
      } catch (e) {
        /* ok */
      }
      let value = this.get('value');
      try {
        value = value.get('id');
      } catch (e) {
        /* ok */
      }
      if (value !== original || value === undefined) {
        this.onChange(this.get('property'), this.get('value'));
      }
    }
  })
});
