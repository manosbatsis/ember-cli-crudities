import Ember from 'ember';
import fetch from 'ember-network/fetch';
import { task } from 'ember-concurrency';
import Cookies from 'ember-cli-js-cookie';

export default Ember.Mixin.create({
  store: Ember.inject.service(),
  modelLoader: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  receiver: this,
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',

  _do_meth(act, receiver, model) {
    const { type, method, text } = act;
    if (type === 'modelMethod') {
      if (model.hasOwnProperty('isFulfilled')) {
        return new Ember.RSVP.Promise((resolve, reject) => {
          model.then((m) => {
            resolve(m[method](act));
            //.then(resolve).catch(reject);
          }).catch((err) => {
            this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
            reject(err);
          });
        });
      } else {
        return model[method](act);
      }
    } else {
      return receiver[method](model, act);
    }
  },

  actionIsRunning: Ember.computed.or('method.isRunning', 'rqst.isRunning'),

  method: task(function * (act, receiver, model) {
    const { text } = act;
    this.set('performing', text);
    yield this._do_meth(act, receiver, model);
    // .finally(() => {
      this.set('performing', false);
    // });
  }),

  doRqst(url, act, model) {
    const { verb, pushPayload, text } = act;
    const params = {
      method: verb,
      headers: {
        "X-CSRFToken": Cookies.get('csrftoken'),
        "Content-Type": 'application/json',
      }, credentials: 'include'
    };
    if (model !== undefined && verb.toUpperCase() === 'POST') {
      params['body'] = JSON.stringify(model.serialize());
    }
    const rv = new Ember.RSVP.Promise((resolve, reject) => {
      fetch(`${url}?format=json`, params).then((response) => {
        if (response.status === 400) {
          response.json().then((json) => {
            reject(json);
          }).catch(() => {
            reject(response);
          });
        } else if (response.status > 400) {
          reject(response);
        } else {
          response.json().then((json) => {
            resolve(json);
          }).catch(() => {
            reject(response);
          });
        }
      });
    });
    if (pushPayload) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((json) => {
          this.get('store').pushPayload(json);
          resolve();
        }, (err) => {
          this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
          reject(err);
        });
      });
    } else {
      return rv.catch((err) => {
        this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
      });
    }
  },

  rqst: task(function * (url, act, model) {
    const { text } = act;
    this.set('performing', text);
    yield this.doRqst(url, act, model).finally(() => {
      this.set('performing', false);
    });
  }),

  _wizard(record, act) {
    const modelLoader = this.get('modelLoader');
    const promises = [];
    const { params } = act;
    params.needs.forEach((need) => {
      promises.push(modelLoader.ensure_model(need.app, need.singular, need.plural));
    });

    promises.push(modelLoader.ensure_model('wizard', params.model, params.model, false));
    Ember.RSVP.all(promises).then(() => {
      this.set('wizardModel', this.get('store').createRecord(`wizard/${params.model}`), {});
      Ember.set(act.params, 'fieldsets', [modelLoader.merge_fields_fieldset(params.fields, {fields: Ember.get(params, 'fieldsets')})]);
      this.set('wizardAct', act);
      this.set('modalRecord', record);
      this.set('wizardOpen', true);
    }, (err) => {
      console.error(err);
      this.get('flashMessages').danger(`An error occured while performing "${act.text}" on ${this.get('model.__str__')}`);
    })
  }
});
