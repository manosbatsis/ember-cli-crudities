import Ember from 'ember';

export function queryString(params/*, hash*/) {
  return params[0].map((record) => {
    return `ids[]=${record.get('id')}`;
  }).join('&');
}

export default Ember.Helper.helper(queryString);
