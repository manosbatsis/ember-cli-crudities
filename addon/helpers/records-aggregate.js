import Ember from 'ember';

export function recordsAggregate(params/*, hash*/) {
  const { type, property_path, group_by } = params[0];
  const models = params[1];

  let rv = 0;
  let label;
  if (group_by) {
    rv= {};
  }

  models.forEach((record) => {
    if (group_by) {
      label = record.get(group_by);
      if (!label) {
        label = '_';
      }
      if (!rv.hasOwnProperty(label)) {
        rv[label] = [0, 0];
      }
    }
    switch(type) {
      case 'count':
        if (group_by) {
          rv[label]++;
        } else {
          rv++;
        }
        break;
      case 'avg':
        if (group_by) {
          rv[label][1]++;
        }
      case 'sum':
        if (group_by) {
          rv[label][0] += parseFloat(record.get(property_path));
        } else {
          rv += parseFloat(record.get(property_path));
        }
    }
  });

  if (type === 'avg') {
    if (group_by) {
      for (let prop in rv) {
        if (rv.hasOwnProperty(prop)) {
          rv[prop][0] = rv[prop][0] / rv[prop][1];
        }
      }
    } else {
      rv =  rv / models.get('length');
    }
  }

  if (group_by) {
    const rrv = [];
    for(let prop in rv) {
      if (rv.hasOwnProperty(prop)) {
        rrv.push(`${prop}: ${rv[prop][0]}`);
      }
    }
    return rrv;
  }

  return [rv];
}

export default Ember.Helper.helper(recordsAggregate);
