# Actions

Actions can be performed either in bulk or per-record mode. There are 3 kinds of actions
supported: `download`, `request` and `modelMethod`.

## Action options common to all actions

| Option | Required | Default | Description |
| ------ | -------- | ------- | ----------- |
| *type* | **Yes** | | The type of action this represents. See [action types](#action-types) |
| *text* | **Yes** | | The "human readable name" of the action. It will be printed on the action button in `change-form`'s and will be used a button title in `change-list`'s |
| *icon_class* | **Yes** | | The icon class of the icon used on the action button. |
| *btn_class* | No | `'btn btn-default'` | The class that should be used on the action button. |
| *allowBulk* | No | `false` | Only for `custom_action`'s. When this is `true`, the action will also be available as a `bulk_action`. A `download` `custom_action` cannot `allowBulk` (you need to create a separate `bulk_action` for it). |
| *display_condition* | No | `undefined` |  A pojo describing the conditions under which this action should be displayed. See [Display_condition options](widgets.md#display_condition-options) |

## Action types

### `download`

Download actions can be either custom or bulk but not both at the same time (a click on
a link will only dosnload a single file. Bulk download is a different url than
`custom_action` download and usualy yield an archive file)

### Options specific to `download` actions

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *url* | **Yes** | | The url to use to perform the action. For `download` `custom_action`'s, the placeholder `:id` will be substituted by the record's `id`. For `download` `bulk_action`'s, the id's of selected records will be passed in as a `GET` parameter `ids[]`. |

### `request`

Request actions can be custom, bulk or both. A request action represents a call made to
the backend (api or other)

### Options specific to `request` actions

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *url* | **Yes** | | The url to use to perform the action. The placeholder `:id` will be substituted by the record's `id`. |
| *verb* | **Yes** | | The verb (`POST`, `GET`, `OPTIONS`, ...) used for the request. |
| *pushPayload* | No | `false` | A boolean representing wether or not the reply to the request will conatin data that should be loaded in Ember data's store using `pushPayload`. |

### `modelMethod`

ModelMethod action can also be custom, bulk or both; they represent a method call made on
the frontend model.

### Options specific to `modelMethod` actions

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *method* | **Yes** | | The name of the method that should be called on the model. |
