# `app-breadcrumbs`

This is a simple breadcrumbs component displaying where you are inside the application structure with links to higher-level pages.

## Example usage

```
## template.hbs

{{app-breadcrumbs
  appRoute='djember.app'
  changeListRoute='djember.list'
  changeFormRoute='djember.form'
}}
{{outlet}}
```


## Options

| Option | Required? | Default | Description | 
| ------ | --------- | ------- | ----------- |
| *appRoute* | **Yes** | | The base route that lists models for a single application, this route will receive a single parameter `app_name` like 'sales' |
| *changeListRoute* | **Yes** | | The base route to use for model lists. This route will receive 2 parameters `app_name` and `model_name` like 'sales' and 'category' |
| *changeFormRoute* | **Yes** | | The base route to use for model forms. This parameter is only used to identify the current route. |
| *mainRoute* | No | `'index'` | The base route that lists applications and models for the whole site |
| *mainLabel* | No | `'Dashboard` | The label used as breadcrumbs root label |
