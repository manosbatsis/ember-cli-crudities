# `app-dashboard`

This component provides a simple list of all applications available as well as, for each
application a list of models they provide. Optionally, it can also provide an "Add" and
a "List" link for each of these models.

## Example usage

```
## route.js

...
  model() {
    return [
      {
        name: 'sales',
        models: [
          {
            singular: 'category',
            name: 'categories'
          }, {
            singular: 'product',
            name: 'products'
          }
        ]
      }, {
        name: 'accounting',
        models: [
          {
            singular: 'invoice',
            name: 'invoices'
          }
        ]
      }
    ];
  }


## template.hbs

{{app-dashboard model=model}}
```

## Options

| Option | Required? | Default | Description | 
| ------ | --------- | ------- | ----------- |
| *model* | No | `undefined` | List of applications and models to render. If not  provided, **app-dashboard** will use [ember-cli-dynamic-model](https://bitbucket.org/levit_scs/ember-cli-dynamic-model) to fetch the information from the api. |
| *changeListRoute* | No | `undefined` | The base route to use for model lists. This route will receive 2 parameters `app_name` and `model_name` like 'sales' and 'category' |
| *changeFormRoute* | No | `undefined` | The base route to use for model forms (add). This route will receive 3 parameters `app_name`, `model_name` and 'new' like 'sales', 'category' and 'new' |
| *appRoute* | No | `undefined` | The base route that lists models for a single application |
| *only* | No | `undefined` | If specified, only shows the model of the application which's name matches `only` |
