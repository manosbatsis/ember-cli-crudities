# `change-list`

This component provides an editable model list.

## Example usage

```
## route.js

...
  model() {
    return {
      model_name: 'sales/category',
      label: 'category',
      app_name: 'sales',
      fields: [
        { 
          name: 'name',
          label: 'Name',
          widget: 'text',
        }
      ],
      list_display: ['name'],
      list_editable: ['name']
    };
  }


## template.hbs

{{change-list
  model_name=model.model_name
  label=model.label
  app_name=model.app_name
  fields=model.fields
  list_display=model.list_display
  list_editable=model.list_editable
}}
```

## Options

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *model_name* | **Yes** | | `'sales/product'`| Full model name. |
| *label* | **Yes** | | `'product'` | Label of the model, used to compute the title of the list. |
| *app_name* | No | `undefined` | `'sales'` | Used as parameter to the `changeFormRoute` |
| *fields* | **Yes** | | `[{ name: 'name', label: 'Name', widget: 'text' }]` | A list of fields and their attributes. You must a least specify the name (as in fieldname) for each field. For more information about the other parameters, please see the [widgets section](./widgets). |
| *list_display* | **Yes** | | `['name']` | The list of fieldnames that will be displayed for each record. (depending on how it is acquired, the content of `fields` might have more fields than `list_display`)|
| *list_editable* | No | `[]` | `['name']` | The list of fields that should be editable directly in the records list.|
| *filter_fields* | No | `[]` | `['status', 'category_id']` | The list of fields on which it is possible to filter the list of records on exact values. For `belongsTo`'s you can also use the form "fieldname_id" depending on your backend requirements. This will be sent to the api as `?fieldname=value`  |
| *search_enabled* | No | `false` | `true` | Wheather or not a search field should be displayed (will be sent to the api as `?search=seachValue`)
| *ordering_fields* | No | `[]` | `['name', 'created']` | A list of fields by which the list should be made sortable. This is an api operation and the information will be sent to the api as `?ordering=(-)value` |
| *sortableBy* | No | `'id'` | `'position'` | A positive integer field by which the record list is orderable. If specified, a handle will be displayed on the left of each record in the list to allow re-ordering |
| *changeFormRoute* | No | `undefined` | `'form'` | The name of the route reponsible for record edition/addition. this route will be passed 3 arguments: "app_name", "model_name" and id as in `'sales'`, `'category'` and `3` |
| *cutom_actions* | No | `undefined` | See [actions](./actions.md) | Actions that can be performed on each record on the list or in bulk mode. |
| *bulk_actions* | No | `undefined` | See [actions](./actions.md) | Actions to be perform in bulk (all selected records) |

